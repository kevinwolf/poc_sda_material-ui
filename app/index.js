import React from 'react'
import { render } from 'react-dom'
import { Router } from 'react-router'
import { Provider } from 'react-redux'
import createBrowserHistory from 'history/lib/createBrowserHistory'
import injectTapEventPlugin from 'react-tap-event-plugin'
import routes from 'routes'
import setupStores from 'utils/setupStores'

// Needed for onTouchTap
// Can go away when react 1.0 release
injectTapEventPlugin()

const history = createBrowserHistory()
const store = setupStores({ }, history)

render((
  <Provider store={store}>
    <Router history={history} children={routes} />
  </Provider>
), document.getElementById('app'))
