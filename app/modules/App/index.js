import React, { Component } from 'react'

import themeDecorator from 'material-ui/lib/styles/theme-decorator'
import ThemeManager from 'material-ui/lib/styles/theme-manager'
import CustomTheme from 'utils/muiTheme'

import styles from './styles.css'
import Sidebar from 'modules/Sidebar'
import Map from 'modules/Map'

export class App extends Component {

  render () {
    return (
      <div className={styles.app}>
        <Sidebar />
        <Map />
      </div>
    )
  }

}

export default themeDecorator(ThemeManager.getMuiTheme(CustomTheme))(App)
