import { PropTypes } from 'react'
import L from 'leaflet'
import 'leaflet-draw'
import { MapComponent } from 'react-leaflet'

import { connect } from 'react-redux'
import { getDrawingState } from 'store/controlState/selectors'
import { bindActionCreators } from 'redux'
import { stopDrawing } from 'store/controlState/actions'
import { setAOI } from 'store/areaOfInterest/actions'
import { aoiPolygon } from 'modules/Map/mapOptions'

class Draw extends MapComponent {

  componentDidMount () {
    this.props.map.on('draw:created', (e) => {
      this.props.map.fitBounds(e.layer)
      this.props.setAOI(e.layer.getLatLngs())
      this.props.stopDrawing()
    })
  }

  componentDidUpdate (pastProps) {
    if (this.drawControl) {
      this.drawControl.disable()
    }

    if (this.props.drawing === 'polygon') {
      this.drawControl = new L.Draw.Polygon(this.props.map, aoiPolygon.options.draw.polygon)
      this.drawControl.enable()
    } else if (this.props.drawing === 'rectangle') {
      this.drawControl = new L.Draw.Rectangle(this.props.map, aoiPolygon.options.draw.rectangle)
      this.drawControl.enable()
    }
  }

  render () {
    return null
  }

}

Draw.propTypes = {
  map: PropTypes.object,
  drawing: PropTypes.string
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ stopDrawing, setAOI }, dispatch)
}

export default connect(getDrawingState, mapDispatchToProps)(Draw)
