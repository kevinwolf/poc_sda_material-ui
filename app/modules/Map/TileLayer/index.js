import L from 'leaflet'
import 'esri-leaflet'
import { MapComponent } from 'react-leaflet'

class TileLayer extends MapComponent {

  componentDidMount () {
    L.esri.basemapLayer('Topographic').addTo(this.props.map)
  }

  render () {
    return null
  }

}

export default TileLayer
