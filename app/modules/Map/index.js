import React, { PropTypes } from 'react'

import { connect } from 'react-redux'
import { getAreaOfInterest } from 'store/areaOfInterest/selectors'

import styles from './styles.css'
import { Map, Polygon } from 'react-leaflet'
import TileLayer from 'modules/Map/TileLayer'
import DrawControl from 'modules/Map/DrawControl'
import { aoiPolygon } from 'modules/Map/mapOptions'

export const LiveMap = ({ AOI }) => {
  return (
    <Map
      className={styles.map}
      center={[ 20, 5 ]}
      zoom={3}
      minZoom={2}
      maxZoom={16}
      maxBounds={[ [ -90, -360 ], [ 90, 360 ] ]}
      zoomControl={false}>
      <TileLayer />
      <DrawControl />
      {AOI && <Polygon positions={AOI} {...aoiPolygon.options.draw.polygon.shapeOptions} />}
    </Map>
  )
}

LiveMap.propTypes = {
  AOI: PropTypes.array
}

export default connect(getAreaOfInterest)(LiveMap)
