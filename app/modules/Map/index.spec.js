import { LiveMap } from './'
import { Map } from 'react-leaflet'
import TileLayer from 'modules/Map/TileLayer'
import DrawControl from 'modules/Map/DrawControl'
import { Polygon } from 'react-leaflet'

const setup = (
  AOI = null
) => {
  const props = { AOI }
  const component = shallowRender(<LiveMap {...props} />)
  const [ tileLayer, drawControl, aoiLayer ] = component.props.children
  return { component, props, tileLayer, drawControl, aoiLayer }
}

describe('COMPONENT: <Map />', () => {
  it('should render a leaflet map', () => {
    const { component } = setup()
    expect(component.type).toBe(Map)
  })

  it('should render the tile layer', () => {
    const { tileLayer } = setup()
    expect(tileLayer.type).toBe(TileLayer)
  })

  it('should include the draw controls', () => {
    const { drawControl } = setup()
    expect(drawControl.type).toBe(DrawControl)
  })

  it('should render the AOI if exists', () => {
    const aoiMock = [
      [ 1, 1 ],
      [ 1, 2 ],
      [ 2, 2 ],
      [ 2, 1 ],
      [ 1, 2 ]
    ]
    const { aoiLayer } = setup(aoiMock)
    expect(aoiLayer).toExist()
    expect(aoiLayer.type).toBe(Polygon)
    expect(aoiLayer.props.positions).toBe(aoiMock)
  })
})
