import L from 'leaflet'

const aoiStyle = {
  weight: 5,
  color: '#F88017',
  fill: false,
  clickable: true
}

export const aoiPolygon = new L.Control.Draw({
  draw: {
    polygon: {
      allowIntersection: false, // Restricts shapes to simple polygons
      drawError: {
        color: '#e1e100', // Color the shape will turn when intersects
        message: '<strong>Oh snap!<strong> you can\'t draw that!' // Message that will show when intersect
      },
      shapeOptions: aoiStyle
    },
    rectangle: {
      shapeOptions: aoiStyle
    }
  }
})
