import React from 'react'

const ResultsPanel = () => {
  return (
    <div>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, quas. Fugit ut non impedit, sunt omnis magni. Quibusdam odio ea at ipsum, laborum tempora pariatur! Quasi consequatur iste impedit ipsam!
      </p>
      <p>Distinctio id tempore natus modi? Tenetur modi dolor nostrum laborum fugiat, nemo recusandae, dignissimos animi molestias sit rem ad! Atque blanditiis suscipit obcaecati, temporibus deleniti ab adipisci recusandae, quae nihil.
      </p>
      <p>At voluptatem iusto quae eveniet dolores veniam maxime sunt quasi iste harum, cum labore consequuntur magnam blanditiis reiciendis? Perspiciatis vero, commodi fugiat eos placeat nisi sint dolorem fuga, architecto ipsa!
      </p>
      <p>Perferendis dicta error culpa iste excepturi ut cupiditate nihil pariatur corporis expedita aliquam in, nostrum atque voluptatem accusantium voluptates rerum eaque eius distinctio deleniti eum quae enim inventore deserunt. Accusamus?
      </p>
      <p>Animi sequi ad, in, laboriosam ab, earum incidunt doloremque perspiciatis voluptatibus maxime voluptate temporibus rem. Maxime fugiat repellat soluta exercitationem vel non alias totam corporis ratione, repudiandae modi, rem ex!
      </p>
      <p>Molestias dolore consectetur corporis hic eveniet porro iste rerum blanditiis vel. Accusantium iure porro labore, illum saepe dolor qui repellat, animi tenetur ratione sequi, necessitatibus excepturi doloribus neque culpa architecto.
      </p>
      <p>Ipsa cupiditate sequi eius ad unde in ullam neque suscipit, iusto iste expedita natus doloribus recusandae odio eos, quas nisi ducimus accusamus odit eum possimus libero laudantium sed molestiae! Distinctio.
      </p>
      <p>Blanditiis repellendus quod aperiam doloremque facilis ullam corporis a eius mollitia laborum commodi ad magnam, officiis unde repellat, est, ducimus deleniti nemo, dicta eos. Modi earum dolore unde reprehenderit cum.
      </p>
      <p>Obcaecati distinctio possimus sapiente vitae esse a modi nihil, tempora eligendi ab fugiat aspernatur sunt magni atque at nesciunt expedita, porro neque ipsam quibusdam mollitia. Impedit et amet facilis, deleniti.
      </p>
      <p>Fugit delectus consectetur et, veritatis, voluptas laborum reprehenderit dolor asperiores in accusantium pariatur quibusdam rerum illum autem blanditiis neque facere fuga, natus. Sequi porro corrupti, numquam tempora tenetur dolorem cumque!
      </p>
    </div>
  )
}

export default ResultsPanel
