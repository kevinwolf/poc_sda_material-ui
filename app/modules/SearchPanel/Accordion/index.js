import React, { Component, PropTypes } from 'react'

import styles from './styles.css'

class Accordion extends Component {

  constructor (props) {
    super(props)
    this.state = {
      open: this.props.open || false
    }

    this._toggleAccordion = this._toggleAccordion.bind(this)
  }

  componentWillReceiveProps (nextProps) {
    this.setState({ open: nextProps.open })
  }

  render () {
    return (
      <div className={`${styles.accordion} ${this.state.open && styles.open}`}>
        <div className={styles.title} onClick={this._toggleAccordion}>
          <span>{this.props.title}</span>
          <div className={styles.icon}>{!this.state.open ? '+' : '—'}</div>
        </div>
        <div className={styles.content}>{this.props.children}</div>
      </div>
    )
  }

  _toggleAccordion () {
    this.setState({ open: !this.state.open })
  }

}

Accordion.propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
  children: PropTypes.node
}

export default Accordion
