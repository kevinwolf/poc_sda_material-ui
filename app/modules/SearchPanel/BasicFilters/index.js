import React from 'react'

import styles from './styles.css'
import { DatePicker, Slider } from 'material-ui'

const BasicFilters = () => {
  return (
    <div>
      <div className={styles.group}>
        <DatePicker autoOk hintText={'Start date'} style={{ width: '100%' }} textFieldStyle={{ width: '100%' }} />
      </div>

      <div className={styles.group}>
        <DatePicker autoOk hintText={'End date'} style={{ width: '100%' }} textFieldStyle={{ width: '100%' }} />
      </div>

      <div className={styles.group}>
        <div className={styles.label}>
          {'Cloud Cover'}
        </div>
        <div className={styles.control}>
          <Slider min={0} max={100} step={1} style={{ margin: 0 }} />
        </div>
      </div>

      <div className={styles.group}>
        <div className={styles.label}>
          {'Off Nadir'}
        </div>
        <div className={styles.control}>
          <Slider min={0} max={100} step={1} style={{ margin: 0 }} />
        </div>
      </div>

      <div className={styles.group}>
        <div className={styles.label}>
          {'Sun Elevation'}
        </div>
        <div className={styles.control}>
          <Slider min={0} max={100} step={1} style={{ margin: 0 }} />
        </div>
      </div>
    </div>
  )
}

export default BasicFilters
