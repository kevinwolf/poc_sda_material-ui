import React, { PropTypes } from 'react'

import { connect } from 'react-redux'
import { createSelector } from 'reselect'
import { getAreaOfInterest } from 'store/areaOfInterest/selectors'
import { getDrawingState } from 'store/controlState/selectors'
import { bindActionCreators } from 'redux'
import { startDrawing } from 'store/controlState/actions'
import { resetAOI } from 'store/areaOfInterest/actions'

import styles from './styles.css'
import { IconButton, SvgIcon } from 'material-ui'
import { ImageCropSquare, FileFileUpload, ImageEdit } from 'material-ui/lib/svg-icons'

export const DrawControls = ({ drawing, AOI, startDrawing, resetAOI }) => {
  const _handlePolygonDraw = () => {
    resetAOI()
    startDrawing('polygon')
  }

  const _handleSquareDraw = () => {
    resetAOI()
    startDrawing('rectangle')
  }

  return (
    <div>
      <IconButton tooltip={'Draw a Polygon'} onClick={_handlePolygonDraw} className={`${drawing === 'polygon' && styles.active}`}>
        <SvgIcon>
          <path d={'M14.927,6.684L5.099,4.976L1.75,10.073v7.898h13.533l8.266-5.748L14.927,6.684z M14.655,15.971H3.749v-5.3 l2.3-3.5l8.136,1.414l5.765,3.704L14.655,15.971z'}/>
          <circle cx={2.916} cy={16.945} r={1.8}/>
          <circle cx={5.94} cy={6.06} r={1.8}/>
          <circle cx={21.748} cy={12.209} r={1.8}/>
          <circle cx={14.655} cy={7.537} r={1.8}/>
          <circle cx={2.916} cy={10.409} r={1.8}/>
          <ellipse cx={14.655} cy={16.945} rx={1.8} ry={1.8}/>
        </SvgIcon>
      </IconButton>
      <IconButton tooltip={'Draw a Rectangle'} onClick={_handleSquareDraw} className={`${drawing === 'rectangle' && styles.active}`}>
        <ImageCropSquare />
      </IconButton>
      <IconButton tooltip={'Upload a Shapefile/KML'}>
        <FileFileUpload />
      </IconButton>
      <IconButton tooltip={AOI && 'Edit Area of Interest'} disabled={!AOI}>
        <ImageEdit />
      </IconButton>
    </div>
  )
}

DrawControls.propTypes = {
  drawing: PropTypes.string,
  AOI: PropTypes.array,
  startDrawing: PropTypes.func,
  resetAOI: PropTypes.func
}

const mapStateToProps = createSelector(
  getDrawingState,
  getAreaOfInterest,
  (drawing, AOI) => ({
    ...drawing,
    ...AOI
  })
)

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ startDrawing, resetAOI }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawControls)
