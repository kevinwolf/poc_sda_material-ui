import { DrawControls } from './'
import { IconButton, SvgIcon } from 'material-ui'
import { ImageCropSquare, FileFileUpload, ImageEdit } from 'material-ui/lib/svg-icons'

const setup = (
  AOI = null,
  startDrawing = expect.createSpy(),
  resetAOI = expect.createSpy()
) => {
  const props = { AOI, startDrawing, resetAOI }
  const component = shallowRender(<DrawControls {...props} />)
  const [ drawPolygon, drawRectangle, uploadFile, editAOI ] = component.props.children
  return { component, props, drawPolygon, drawRectangle, uploadFile, editAOI }
}

describe('COMPONENT: <DrawControls />', () => {
  describe('the draw polygon button', () => {
    it('should exist', () => {
      const { drawPolygon } = setup()
      expect(drawPolygon.type).toBe(IconButton)
      expect(drawPolygon.props.tooltip).toBe('Draw a Polygon')
      expect(drawPolygon.props.children.type).toBe(SvgIcon)
    })

    describe('when clicked', () => {
      it('should reset the AOI', () => {
        const { drawPolygon, props } = setup()
        drawPolygon.props.onClick()
        expect(props.resetAOI).toHaveBeenCalled()
      })

      it('should start drawing a polygon on the map', () => {
        const { drawPolygon, props } = setup()
        drawPolygon.props.onClick()
        expect(props.startDrawing).toHaveBeenCalledWith('polygon')
      })
    })
  })

  describe('the draw rectangle button', () => {
    it('should exist', () => {
      const { drawRectangle } = setup()
      expect(drawRectangle.type).toBe(IconButton)
      expect(drawRectangle.props.tooltip).toBe('Draw a Rectangle')
      expect(drawRectangle.props.children.type).toBe(ImageCropSquare)
    })

    describe('when clicked', () => {
      it('should reset the AOI', () => {
        const { drawRectangle, props } = setup()
        drawRectangle.props.onClick()
        expect(props.resetAOI).toHaveBeenCalled()
      })

      it('should start drawing a polygon on the map', () => {
        const { drawRectangle, props } = setup()
        drawRectangle.props.onClick()
        expect(props.startDrawing).toHaveBeenCalledWith('rectangle')
      })
    })
  })

  describe('the upload file button', () => {
    it('should exist', () => {
      const { uploadFile } = setup()
      expect(uploadFile.type).toBe(IconButton)
      expect(uploadFile.props.tooltip).toBe('Upload a Shapefile/KML')
      expect(uploadFile.props.children.type).toBe(FileFileUpload)
    })
  })

  describe('the edit AOI', () => {
    it('should exist', () => {
      const { editAOI } = setup()
      expect(editAOI.type).toBe(IconButton)
      expect(editAOI.props.children.type).toBe(ImageEdit)
    })
  })

  it('should be disabled when there is not a drawn AOI', () => {
    const { editAOI } = setup()
    expect(editAOI.props.disabled).toBe(true)
  })

  it('should enabled when there is a drawn AOI', () => {
    const { editAOI } = setup({ type: 'Polygon', coordinates: [] })
    expect(editAOI.props.disabled).toBe(false)
  })
})
