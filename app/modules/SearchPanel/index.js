import React, { PropTypes } from 'react'

import { connect } from 'react-redux'

import Accordion from 'modules/SearchPanel/Accordion'
import DrawControls from 'modules/SearchPanel/DrawControls'
import BasicFilters from 'modules/SearchPanel/BasicFilters'
import { TextField } from 'material-ui'

export const SearchPanel = ({ AOI }) => {
  const _handleTextChange = (e) => e.stopPropagation()

  return (
    <div>
      <Accordion title={'Define your area of interest'} open>
        <DrawControls />
      </Accordion>
      <Accordion title={'Filters'} open={!!AOI}>
        <BasicFilters />
      </Accordion>
      <Accordion title={'Search by ID'}>
        <TextField
          fullWidth
          multiLine
          rows={1}
          rowsMax={8}
          floatingLabelText={'Enter IDs'}
          hintText={'Separated by comma, space or new line'}
          onChange={_handleTextChange} />
      </Accordion>
    </div>
  )
}

SearchPanel.propTypes = {
  AOI: PropTypes.array
}

const mapStateToProps = (state) => ({
  AOI: state.areaOfInterest
})

export default connect(mapStateToProps)(SearchPanel)
