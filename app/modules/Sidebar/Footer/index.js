import React, { PropTypes } from 'react'
import { Paper, RaisedButton } from 'material-ui'

import { createSelector } from 'reselect'
import { connect } from 'react-redux'
import { getActiveTab } from 'store/controlState/selectors'
import { getAreaOfInterest } from 'store/areaOfInterest/selectors'

import styles from './styles.css'

export const Footer = (props) => {
  const { activeTab, AOI } = props

  return (
    <Paper className={styles.footer}>
      {activeTab === 1
        ? <RaisedButton primary label={'Search'} disabled={!AOI} />
        : <RaisedButton primary label={'Submit for Quote'} />
      }
    </Paper>
  )
}

Footer.propTypes = {
  activeTab: PropTypes.number,
  AOI: PropTypes.array
}

const mapStateToProps = createSelector(
  getActiveTab,
  getAreaOfInterest,
  (activeTab, AOI) => {
    return { ...activeTab, ...AOI }
  }
)

export default connect(mapStateToProps)(Footer)
