import { Footer } from './'
import { Paper, RaisedButton } from 'material-ui'

const setup = (
  activeTab = 1,
  AOI = null
) => {
  const props = { activeTab, AOI }
  const component = shallowRender(<Footer {...props} />)
  const button = component.props.children
  return { component, props, button }
}

describe('COMPONENT: <Footer />', () => {
  it('should render a container with a button', () => {
    const { component, button } = setup()
    expect(component.type).toBe(Paper)
    expect(button.type).toBe(RaisedButton)
  })

  describe('the search button', () => {
    it('should be shown when the active tab is the search one', () => {
      const { button } = setup()
      expect(button.props.label).toBe('Search')
    })

    it('should be disabled if there is not any drawn AOI', () => {
      const { button } = setup()
      expect(button.props.disabled).toBe(true)
    })

    it('should be enabled if there is a drawn AOI', () => {
      const { button } = setup(1, [
        [ 1, 1 ],
        [ 1, 2 ],
        [ 2, 2 ],
        [ 2, 1 ],
        [ 1, 2 ]
      ])
      expect(button.props.disabled).toBe(false)
    })
  })

  describe('the submit for quote button', () => {
    it('should be shown when the active tab is the results one', () => {
      const { button } = setup(2)
      expect(button.props.label).toBe('Submit for Quote')
    })
  })
})
