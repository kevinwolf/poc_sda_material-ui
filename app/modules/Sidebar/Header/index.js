import React from 'react'
import { AppBar } from 'material-ui'

import styles from './styles.css'
import dgLogo from './dg-logo.svg'

const Header = () => {
  return (
    <AppBar
      className={styles.header}
      showMenuIconButton={false}
      zDepth={1}>
      <img src={dgLogo} className={styles.logo} />
    </AppBar>
  )
}

export default Header
