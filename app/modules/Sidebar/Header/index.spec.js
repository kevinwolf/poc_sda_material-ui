import Header from './'
import { AppBar } from 'material-ui'

const setup = (

) => {
  const component = shallowRender(<Header />)
  return { component }
}

describe('COMPONENT: <Header />', () => {
  it('should render a material appbar with the logo', () => {
    const { component } = setup()
    expect(component.type).toBe(AppBar)
    expect(component.props.children.type).toBe('img')
  })
})
