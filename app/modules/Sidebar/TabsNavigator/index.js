import React, { PropTypes } from 'react'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { changeActiveTab } from 'store/controlState/actions'
import { getActiveTab } from 'store/controlState/selectors'

import styles from './styles.css'
import { Tabs, Tab } from 'material-ui'
import { ActionSearch, NavigationMenu, ActionSettings } from 'material-ui/lib/svg-icons'
import SearchPanel from 'modules/SearchPanel'
import ResultsPanel from 'modules/ResultsPanel'
import OptionsPanel from 'modules/OptionsPanel'

export const TabsNavigator = ({ activeTab, changeActiveTab }) => {
  const _handleTabChange = (value) => {
    changeActiveTab(value)
  }

  return (
    <Tabs
      className={styles.tabs}
      contentContainerClassName={styles.content}
      tabItemContainerStyle={{ backgroundColor: '#333333' }}
      value={activeTab}
      onChange={_handleTabChange}>
      <Tab value={1} icon={<ActionSearch />}>
        <SearchPanel />
      </Tab>
      <Tab value={2} icon={<NavigationMenu />}>
        <ResultsPanel />
      </Tab>
      <Tab value={3} icon={<ActionSettings />}>
        <OptionsPanel />
      </Tab>
    </Tabs>
  )
}

TabsNavigator.propTypes = {
  activeTab: PropTypes.number,
  changeActiveTab: PropTypes.func
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ changeActiveTab }, dispatch)
}

export default connect(getActiveTab, mapDispatchToProps)(TabsNavigator)
