import { TabsNavigator } from './'
import { Tabs, Tab } from 'material-ui'
import SearchPanel from 'modules/SearchPanel'
import ResultsPanel from 'modules/ResultsPanel'
import OptionsPanel from 'modules/OptionsPanel'

const setup = (
  activeTab = 1,
  changeActiveTab = expect.createSpy()
) => {
  const props = { activeTab, changeActiveTab }
  const component = shallowRender(<TabsNavigator {...props} />)
  const tabsList = component.props.children
  return { component, props, tabsList }
}

describe('COMPONENT: <TabsNavigator />', () => {
  it('should render three tabs', () => {
    const { component, tabsList } = setup()
    expect(component.type).toBe(Tabs)
    expect(tabsList.length).toBe(3)
    tabsList.forEach((tab) => expect(tab.type).toBe(Tab))
  })

  it('should set the passed tab', () => {
    const { component } = setup(2)
    expect(component.props.value).toBe(2)
  })

  it('should change the active tab when clicking on a tab', () => {
    const { component, props } = setup()
    component.props.onChange(2)
    expect(props.changeActiveTab).toHaveBeenCalledWith(2)
  })

  it('should render the search panel', () => {
    const { tabsList } = setup()
    expect(tabsList[0].props.children.type).toBe(SearchPanel)
  })

  it('should render the results panel', () => {
    const { tabsList } = setup()
    expect(tabsList[1].props.children.type).toBe(ResultsPanel)
  })

  it('should render the options panel', () => {
    const { tabsList } = setup()
    expect(tabsList[2].props.children.type).toBe(OptionsPanel)
  })
})
