import React from 'react'

import styles from './styles.css'
import { LeftNav } from 'material-ui'
import Header from 'modules/Sidebar/Header'
import TabsNavigator from 'modules/Sidebar/TabsNavigator'
import Footer from 'modules/Sidebar/Footer'

const Sidebar = () => {
  return (
    <LeftNav
      docked
      className={styles.sidebar}
      width={390}>
      <Header />
      <TabsNavigator />
      <Footer />
    </LeftNav>
  )
}

export default Sidebar
