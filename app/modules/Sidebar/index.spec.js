import Sidebar from './'
import { LeftNav } from 'material-ui'
import Header from 'modules/Sidebar/Header'
import TabsNavigator from 'modules/Sidebar/TabsNavigator'
import Footer from 'modules/Sidebar/Footer'

const setup = (

) => {
  const props = {}
  const component = shallowRender(<Sidebar {...props} />)
  const [ header, tabs, footer ] = component.props.children
  return { component, header, tabs, footer }
}

describe('COMPONENT: <Sidebar />', () => {
  it('should render the sidebar component', () => {
    const { component } = setup()
    expect(component.type).toBe(LeftNav)
  })

  it('should render the header', () => {
    const { header } = setup()
    expect(header.type).toBe(Header)
  })

  it('should render the tabs', () => {
    const { tabs } = setup()
    expect(tabs.type).toBe(TabsNavigator)
  })

  it('should render the footer button', () => {
    const { footer } = setup()
    expect(footer.type).toBe(Footer)
  })
})
