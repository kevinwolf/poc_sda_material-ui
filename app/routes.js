import React from 'react'
import { Route } from 'react-router'

import App from 'modules/App'

export default (
  <Route path='/' component={App} />
)
