
export const SET_AOI = 'areaOfInterest/SET_AOI'
export function setAOI (latLngs) {
  return {
    type: SET_AOI,
    payload: { latLngs }
  }
}

export const RESET_AOI = 'areaOfInterest/RESET_AOI'
export function resetAOI () {
  return {
    type: RESET_AOI
  }
}
