import * as actions from './actions'

describe('ACTION CREATORS: areaOfInterest', () => {
  const latLngs = [
    [ 1, 1 ],
    [ 1, 2 ],
    [ 2, 2 ],
    [ 2, 1 ],
    [ 1, 2 ]
  ]

  it('should create the setAOI action', () => {
    expect(
      actions.setAOI(latLngs)
    ).toEqual({
      type: actions.SET_AOI,
      payload: { latLngs }
    })
  })

  it('should create the resetAOI action', () => {
    expect(
      actions.resetAOI()
    ).toEqual({
      type: actions.RESET_AOI
    })
  })
})
