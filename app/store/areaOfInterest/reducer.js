import * as actions from './actions'

const defaultState = null
export default function areaOfInterest (state = defaultState, { type, payload }) {
  switch (type) {
    case actions.SET_AOI:
      return payload.latLngs

    case actions.RESET_AOI:
      return defaultState

    default:
      return state
  }
}
