import reducer from './reducer'
import * as actions from './actions'

describe('REDUCER: areaOfInterest', () => {
  const aoiMock = [
    [ 1, 1 ],
    [ 1, 2 ],
    [ 2, 2 ],
    [ 2, 1 ],
    [ 1, 2 ]
  ]

  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).toEqual(null)
  })

  it('should handle the setAOI action', () => {
    const stateBefore = null
    const stateAfter = aoiMock

    expect(
      reducer(stateBefore, actions.setAOI(aoiMock))
    ).toEqual(stateAfter)
  })

  it('should handle the reset action', () => {
    const stateBefore = aoiMock
    const stateAfter = null

    expect(
      reducer(stateBefore, actions.resetAOI())
    ).toEqual(stateAfter)
  })
})
