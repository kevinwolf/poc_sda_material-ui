import { createSelector } from 'reselect'

export const getAreaOfInterest = createSelector(
  (state) => state.areaOfInterest,
  (AOI) => {
    return { AOI }
  }
)
