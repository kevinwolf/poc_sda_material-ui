import * as selectors from './selectors'

describe('SELECTORS: areaOfInterest', () => {
  it('should select getAreaOfInterest', () => {
    const stateAfter = [
      [ 1, 1 ],
      [ 1, 2 ],
      [ 2, 2 ],
      [ 2, 1 ],
      [ 1, 2 ]
    ]
    const stateBefore = [
      [ 1, 1 ],
      [ 1, 2 ],
      [ 2, 2 ],
      [ 2, 1 ],
      [ 1, 2 ]
    ]

    expect(
      selectors.getAreaOfInterest({ areaOfInterest: stateAfter })
    ).toEqual({ AOI: stateBefore })
  })
})
