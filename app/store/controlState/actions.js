
export const CHANGE_ACTIVE_TAB = 'controlState/CHANGE_ACTIVE_TAB'
export function changeActiveTab (tab) {
  return {
    type: CHANGE_ACTIVE_TAB,
    payload: { tab }
  }
}

export const START_DRAWING = 'controlState/START_DRAWING'
export function startDrawing (shape) {
  return {
    type: START_DRAWING,
    payload: { shape }
  }
}

export const STOP_DRAWING = 'controlState/STOP_DRAWING'
export function stopDrawing () {
  return {
    type: STOP_DRAWING
  }
}
