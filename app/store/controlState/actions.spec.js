import * as actions from './actions'

describe('ACTION CREATORS: controlState', () => {
  it('should create the changeActiveTab action', () => {
    expect(
      actions.changeActiveTab(2)
    ).toEqual({
      type: actions.CHANGE_ACTIVE_TAB,
      payload: {
        tab: 2
      }
    })
  })

  it('should create the startDrawing action', () => {
    expect(
      actions.startDrawing('polygon')
    ).toEqual({
      type: actions.START_DRAWING,
      payload: {
        shape: 'polygon'
      }
    })
  })

  it('should create the stopDrawing action', () => {
    expect(
      actions.stopDrawing()
    ).toEqual({
      type: actions.STOP_DRAWING
    })
  })
})
