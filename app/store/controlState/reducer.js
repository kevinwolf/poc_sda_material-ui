import * as actions from './actions'

const defaultState = {
  activeTab: 1,
  drawing: null
}

export default function controlState (state = defaultState, { type, payload }) {
  switch (type) {
    case actions.CHANGE_ACTIVE_TAB:
      return { ...state, activeTab: payload.tab }

    case actions.START_DRAWING:
      return { ...state, drawing: payload.shape }

    case actions.STOP_DRAWING:
      return { ...state, drawing: null }

    default:
      return state
  }
}
