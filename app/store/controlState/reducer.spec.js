import reducer from './reducer'
import * as actions from './actions'

describe('REDUCER: controlState', () => {
  const defaultState = {
    activeTab: 1,
    drawing: null
  }

  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).toEqual(defaultState)
  })

  it('should handle the changeActiveTab action', () => {
    const stateBefore = { ...defaultState, activeTab: 1 }
    const stateAfter = { ...defaultState, activeTab: 2 }

    expect(
      reducer(stateBefore, actions.changeActiveTab(2))
    ).toEqual(stateAfter)
  })

  it('should handle the startDrawing action', () => {
    const stateBefore = { ...defaultState, drawing: false }
    const stateAfter = { ...defaultState, drawing: 'polygon' }

    expect(
      reducer(stateBefore, actions.startDrawing('polygon'))
    ).toEqual(stateAfter)
  })

  it('should handle the stopDrawing action', () => {
    const stateBefore = { ...defaultState, drawing: 'polygon' }
    const stateAfter = { ...defaultState, drawing: null }

    expect(
      reducer(stateBefore, actions.stopDrawing())
    ).toEqual(stateAfter)
  })
})
