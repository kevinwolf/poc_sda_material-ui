import { createSelector } from 'reselect'

export const getActiveTab = createSelector(
  (state) => state.controlState,
  (controlState) => {
    return {
      activeTab: controlState.activeTab
    }
  }
)

export const getDrawingState = createSelector(
  (state) => state.controlState,
  (controlState) => {
    return {
      drawing: controlState.drawing
    }
  }
)
