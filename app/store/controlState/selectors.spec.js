import * as selectors from './selectors'

describe('SELECTORS: controlState', () => {
  const defaultState = {
    activeTab: 1,
    drawing: null
  }

  it('should select getActiveTab', () => {
    expect(
      selectors.getActiveTab({ controlState: defaultState })
    ).toEqual({ activeTab: 1 })
  })

  it('should select getDrawingState', () => {
    expect(
      selectors.getDrawingState({ controlState: defaultState })
    ).toEqual({ drawing: null })
  })
})
