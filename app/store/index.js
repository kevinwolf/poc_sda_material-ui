import { combineReducers } from 'redux'
import controlState from './controlState/reducer'
import areaOfInterest from './areaOfInterest/reducer'

export default combineReducers({
  controlState,
  areaOfInterest
})
