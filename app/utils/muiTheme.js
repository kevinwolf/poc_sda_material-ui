import Colors from 'material-ui/lib/styles/colors'

export default {
  palette: {
    primary1Color: '#0d3b5e',
    primary2Color: Colors.lightBlue700,
    accent1Color: '#70993f'
  }
}
