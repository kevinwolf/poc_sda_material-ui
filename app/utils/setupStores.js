import { createStore, applyMiddleware, compose } from 'redux'
import { syncHistory } from 'react-router-redux'
import thunk from 'redux-thunk'
import rootReducer from 'store'

export default function configureStore (initialState, history) {
  const middleware = [ thunk, syncHistory(history) ]

  if (process.env.NODE_ENV === 'development') {
    // const createLogger = require('redux-logger')
    // middleware.push(createLogger())
  }

  const store = createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(...middleware),
      window.devToolsExtension ? window.devToolsExtension() : (f) => f
    )
  )

  if (module.hot) {
    module.hot.accept('../store', () => {
      store.replaceReducer(require('../store').default)
    })
  }

  return store
}
