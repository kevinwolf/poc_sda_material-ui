'use strict'

var path = require('path')
var webpack = require('webpack')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var pkg = require('../package.json')
var ignoreVendors = [ 'express', 'colors', 'compression', 'logger', 'morgan', 'body-parser', 'mongoose' ]

module.exports = {
  entry: {
    app: [
      path.resolve(__dirname, '../app')
    ],
    vendor: Object.keys(pkg.dependencies).filter(function (vendor) { return !~ignoreVendors.indexOf(vendor) })
  },
  resolve: {
    modulesDirectories: [ 'node_modules', '../app' ],
    extensions: [ '', '.js', '.jsx' ]
  },
  output: {
    path: path.resolve(__dirname, '../build'),
    filename: '[name].bundle.js',
    publicPath: '/'
  },
  module: {
    preLoaders: [
      { test: /\.js$/, loader: 'eslint', exclude: /node_modules/ }
    ],
    loaders: [
      { test: /\.(js|jsx)$/, loader: 'babel', exclude: /node_modules/ },
      { test: /\.(otf|eot|svg|ttf|woff|woff2|png|gif)$/, loader: 'url-loader?limit =8192' }
    ]
  },
  postcss: function () {
    return [ require('stylelint'), require('postcss-cssnext'), require('postcss-reporter') ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
    }),
    new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.bundle.js'),
    new HtmlWebpackPlugin({
      inject: 'body',
      template: path.resolve(__dirname, '../app/index.html')
    })
  ]
}
