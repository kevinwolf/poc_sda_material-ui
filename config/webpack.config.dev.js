'use strict'

var base = require('./webpack.config.base.js')

module.exports = Object.assign({}, base, {
  devtool: 'eval-source-map',
  entry: Object.assign(base.entry, {
    app: base.entry.app.concat(
      'webpack-dev-server/client?http://localhost:3000',
      'webpack/hot/only-dev-server'
    )
  }),
  module: Object.assign(base.module, {
    loaders: base.module.loaders.concat({
      test: /\.css$/,
      loader: 'style!css?modules&importLoaders =1&localIdentName =[name]__[local]___[hash:base64:5]!postcss'
    })
  })
})
