'use strict'

var base = require('./webpack.config.base.js')
var webpack = require('webpack')
var ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = Object.assign({}, base, {
  module: Object.assign(base.module, {
    loaders: base.module.loaders.concat({
      test: /\.css$/,
      loader: ExtractTextPlugin.extract('style', 'css?modules&importLoaders =1&localIdentName =[name]__[local]___[hash:base64:5]!postcss')
    })
  }),
  plugins: base.plugins.concat(
    new ExtractTextPlugin('styles.css', { allChunks: true }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    })
  )
})
